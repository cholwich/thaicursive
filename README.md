## Thai Cursive Handwriting Dataset

This dataset is collected from 10 Thai natives. Each subject was requested to
write 50 words in his/her own way of writing. Each word is written twice. We
collected x,y coordinates and tagged each stroke with a Thai characters.

This dataset is developed by Pimolluck Jirakunkanok, Cholwich Nattee and
Nirattaya Khamsemanan at Sirindhorn International Institute of Technology,
Thammasat University, Thailand.

![Creative Commons License](http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png)  
Thai Cursive Handwriting Dataset is licensed under a [Creative Commons
Attribution-NonCommercial-NoDerivs 3.0
Unported License](http://creativecommons.org/licenses/by-nc-nd/3.0/).
